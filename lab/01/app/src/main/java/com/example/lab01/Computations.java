package com.example.lab01;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;


public class Computations extends AsyncTask<Void, Float, Void> {
    private WeakReference<Activity> activity_;

    public Computations(Activity activity) {
        activity_ = new WeakReference<>(activity);
    }

    Activity getActivity() {
        return activity_.get();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        final TextView textView = getActivity().findViewById(R.id.text_view);
        textView.setText("Computations in progress ...");
    }

    @Override
    protected void onPostExecute(Void emptiness) {
        super.onPostExecute(emptiness);
        final TextView textView = getActivity().findViewById(R.id.text_view);
        textView.setText("Computations succeeded.");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            int n = 100;
            int whole_time = 3000;
            for (int i = 0; i < n; ++i)
            {
                publishProgress((float)i/n);
                Thread.sleep(whole_time/n);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        final TextView textView = getActivity().findViewById(R.id.text_view);
        textView.setText("Computations interrupted.");
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        final ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar);
        progressBar.setProgress(Math.round(values[0] * 100));
    }
}
