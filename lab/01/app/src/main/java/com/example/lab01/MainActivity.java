package com.example.lab01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.atomic.AtomicReference;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button start_button = (Button) findViewById(R.id.start_button);
        final Button end_button = (Button) findViewById(R.id.end_button);

        AtomicReference<Computations> computations = new AtomicReference<>(
                new Computations(this)
        );
        start_button.setOnClickListener(v -> {
            AsyncTask.Status status = computations.get().getStatus();
            if (!status.equals(AsyncTask.Status.RUNNING)) {
                computations.set(new Computations(this));
                computations.get().execute();
            }
        });


        end_button.setOnClickListener(v -> {
                    if (computations.get().getStatus().equals(AsyncTask.Status.RUNNING)) {
                        computations.get().cancel(true);
                    }
                }
        );
    }
}