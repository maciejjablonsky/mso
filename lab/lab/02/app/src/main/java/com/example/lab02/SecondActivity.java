package com.example.lab02;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends Activity {
    @Override
    protected void onCreate(Bundle state){
       super.onCreate(state);
       setContentView(R.layout.activity_second);

       final TextView textView = (TextView) findViewById(R.id.second_text_view);
       Integer number = getIntent().getIntExtra("extraNumber", -1);
       Log.d("ZX", "Jakas liczba: " + number);
       textView.setText("Passed number from main activity: " + Integer.toString(number));
    }
    @Override
    public void onBackPressed()
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("Result", "I'm your result from second activity");
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
