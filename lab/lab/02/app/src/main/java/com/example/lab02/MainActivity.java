package com.example.lab02;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button startSecondActivityButton = (Button) findViewById(R.id.start_second_activity);

        Intent secondActivityIntent = new Intent(this, SecondActivity.class);
        secondActivityIntent.putExtra("extraNumber", 10);

        startSecondActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(secondActivityIntent, 999);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final TextView textView = (TextView) findViewById(R.id.main_text_view);
        if (requestCode == 999)
        {
            if (resultCode == RESULT_OK){
                Log.d("ZX", "Result is OK: " + data.getStringExtra("Result"));
                textView.setText(data.getStringExtra("Result"));
            }
        }
    }
}